#!/usr/bin/env bash

FILE="./.env"

echo > $FILE

echo input lang
read lang

echo "LANGUAGE=${lang}" >> $FILE

echo input locale
read locale

echo "LOCALE=${locale}" >> $FILE

# echo "input shop => own, shopee"
# read shop

# echo "SHOP=${shop}" >> $FILE