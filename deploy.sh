#!/bin/bash

git reset --hard;
git pull
npm install --unsafe-perm=true --allow-root;
pm2 delete next;
pm2 start npm --name "next" -- run pro;
exit;