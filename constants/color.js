export const SIG_COLOR = '#e60012';
export const SIG_BOLD_COLOR = '#990000';
export const BORDER_LIGHT_COLOR = '#eee';
export const BORDER_LIGHT = `1px solid ${BORDER_LIGHT_COLOR}`;
export const SILVER_COLOR = '#999';
export const BLACK_COLOR = '#333';