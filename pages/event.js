import Carousel from "../components/Carousel";
import Navigation from "../components/Nav/Navigation";

export default function Event() {
  return (
    <div>
      <Navigation />
      <div>event banner</div>
      <Carousel />
    </div>
  );
}