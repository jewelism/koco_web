import Router from 'next/router';
import Navigation from "../components/Nav/Navigation";
import HorizontalMenu from "../components/Nav/HorizontalMenu";

const rankingMenuList = [
  { text: '전체' },
  { text: '여성' },
  { text: '남성' },
  { text: '뷰티' },
  { text: '라이프' },
];

const productList = [
  {
    productId: 2,
    productImage: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIANcAWgMBEQACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABgcBBAUCA//EAEAQAAIBAwIDBAYHBwEJAAAAAAECAwAEEQUhBhIxEyJBUQcUYXGBkRUjMkJSodFicoKSsbLBwhYlNmNzdKKjs//EABsBAQACAwEBAAAAAAAAAAAAAAAEBgEDBQIH/8QAMhEAAgEDAgQDBgYDAQAAAAAAAAECAwQREiEFQVFhEzFxgZGhscHwFSIjMjThFDPR8f/aAAwDAQACEQMRAD8AvGgFAKArr0l69rdrqthpHD10ttNcwu0kpUHkwV3wRvtkbfi91aLm4hb0nUn5G6hRlWmoRORw/qPEOk6tZw+uTX9m5Eb2rL2kjDPek5tiCM5O2PDyNQbLiLuJ6cZ+GPn8WS7qyVGOrP8AZYHFnECcP6U12YGmlZljhiwQHdmCgFsEDr8ga6kpKMXJ8jnxTk8Ihugcf6x9OJa6/p8MVpcyARTRB/q8nADbH2DfG/j0FR6N3RrbRe5uq21WnvJFmg5qSaDNAKAUAoDB6UBy9P1uC+1K/s4yq+puIyWYZkONyB+EHu58wfZnCknnHIy4tYb5nQW6t2fs1niL/hDjPyrJgiHGnDEEwbXbd7g3loTMyK+RKgXDDHny9PaAKjXdHx6Eqa5m+2q+FVjM53B17BHxKsUmG9dtD6u+dsocsP4gyn+A1yuBbRqQfmmvqdDi27hLkdP0gXyt6hpcRDSyyi4lA+5HHuCfe3KPn5VO4rVVO1kn5vZEXh9NzuItciLW+gahxXbPJZrBFYxziNZZgeabfEjJjoAMgH7xBGw3MLhnDXTiqs3u8bf9JN9eqUnTitlzLZUYrunJPVAKAUAoDDdKAqjjXTlg1iS2mRHWdfWQxUbksQfDwwPmKjuzoyqeK08+rO7wtQr05UprOPIiMqfRYM5to5raIhp1aFAyJ4srAA5HXqehxjY1quLFSjmk3GXq/qb61Hwo6tOYrzXbsWHBxjcWOlva3dlcahOVK20sTJ9aCNg+SMEeJAOQM9dqjWvFKc4fq7SXmcyvw+cZfp7p+RGYbGT1KytXeSGW1SPkki35WUYDKdsHrt0I23FcRXfhVpV6b82+3sxzOs7fxKapVOR89VD2WnXMomurm7uOUS3DtzysMgEj91SSABjyG9e6FxK/uouu/wAq5cjxUoxtbdqn5stSx1PQrDQreW1vrKHS4YFEUnbKEWMDA3z5VbitnBk9JWnySkaXpeqajAOtxBEqRn90yMvN8NqjVby3ovTOaTN9O1rVFmMSS6FrljrtobmwdiFYpJHIpV4nH3WU7g7j55qRGSksx3RplFxeGtzpVkwKAUAoDicS8PQ67HETKYLiHPZTKucA9VI8QcDy6dRWTfb3E7eeuBW+vaTNZOtnZ6laXV9JOsBgSFu0XmGebkydvftnqeteKk3GOUsvodT8XnNadKXf+jFnpzWKQWzNcCW0Yo6TueZe6dvzUjwx02606+c4VJxqxSb6Ey0UJQjKDzg3ZHCLlvMAY8zXOhFy8ibJpGvc9m80cZnRZMgiMvyt7x4+fs2NSaClGDlp268vaaKji5KLlv0NJ7TS7Se5u7yC0DRqJXuXiQNvnqQOu3Wt3iV6sIxpt77YyzVopQk5TS23zg0Uub3UZ1mmM9ratkxQKShI8GYjfJ/DkY8j1qxWnCqNKH6kdUueTNKFStJSnlRfkvv5HV0S8v7TXlGj3MLX8yCOSGdsiRAe7zKDnI3wRvjOcjpMpW8aO1P9vT/hB4pSoxw9X5i1tIivorT/AHnNFLcsxZzCCEHsGfCt6OK8cjfoYFAKA8uiyKVcZU9RQET4w060sIrXWY4BDDYO8l1JbKFkWIqQzjHXGxI8s0XnkdiLXWp/S2t6lfxqY7QmOOHnGCQq7sR4ZyPgBVV41OFWrBQ32LBwyEqcJORrI7SyPOYyY4lPZr4sfE/4+fw57hGnFQzu/MnKTk9WNl5HU0DWNKg4f1Kz1max7WSSTFqW5biZjkjCndtuQKR5eyrdaujG2jpf5UitXEasq7yt87Ee4b4LXiK7uIr/AFOVpbGKMRdqnaEklxk7gHdCR4+0Vr4fKE6euKS9PRfQ3XMp0aqUt/X2kptvR3e84judZQQZyzQwN2jD2FnYL+f+a6OTa+LVtGmKw+v/AKTXTNH0/S7JLSxtY4oUIbGMkt+Ik7lvad6wcxtt5Zv0MCgFAKAHpQFf8X67FrDyaPZu5s051vn5SolweXswfFchuYjyx4muTxK/dCGmn+5/D+zo2Np4stU/2r4nD7JVthFbAKp6MN8eZ99VhTfiOdTzO84rRpge2IggyFZgi7AdSK14dWpu/M95UI+hroxmlDrBH2wGzt9xemc+Odz7vEVJklCOnU9PTqaU3OWUtzY0K5Ok8TLzSIgvYQBM7Ed9GZiAo6kiQ432CnORkjvcFrqpTlDyw/h9o4/FKTjOMuxadrKJoVkHLuPusG/MV2jln2oBQCgFAKA4vGV9Pp3DGo3dpL2U8cXcfGSpJA2Hi2+w8TivMnhNnqKzJIqiCxnSBrW0kKqy85mmBk7xHtPe33O+7Ek+RqlW4g5eLU3f3v6/JFihSko6IbL7wdiNUggVWbKoveY7Z8yf61ypydSeVzJyShE0tPuLYQCU3KySzYdsPzHJ8AB5Zx8KnV7a5lPQqbwvLYi0q9CMdTmt+4uJb21Prt5pt7a6YAqxyvCR2jMcAkdUGcY5+X86mPhVaNHLWZP4IjLiFJ1cZ2XxGq2r3+nhol5biNhNDzD76+Bz4EZHxqDZ3H+Jcp528mSrmj/kUMc/NFhcD6jb3uiwNbiNEZQyqhyem/NuSDnbfHSrsVdkkoYFAKAUAoCI+klbibh544E7sdxDLIxP3VdSTt4AbknbatNwm6MkujNtB4qxfchZllg0ztIVR3C5UO3Iu/TJxt4CqbClGvdKm9svH37SyyqulQc/PBNLDgqJuVtZujd75MEackJ9jDcsPYTg+IqzWvC6Fu9S3fVnCr39WsseSJYFCgBdgOgHhXSIRyuLbX1zhfVrfGTJZygD28px+dMZBXOnTLcadbToByywo45RgYIBr59cRca0ovqy40paqUWuh1vR1fNY3ep6fPIqW/rjGHnL4HOFfAOOUd5m2JBq6cOq+Lawk+mPcVi9p+HXkkWPU0iigFAKAUBr6jai9sLm1LconiaMtjOOYEdPHrWU8AgVz6Pb65sQl1qkReLdYYYCI5CPx5bJ92QB45rmUuGUqTlKLep8+noTql/Ookmtly6k50u2mtLKGG4neeVECvI7ZLkDGfZnriukQTcoDW1LH0fc83Tsnz8jQFRcNbcOaV/2cP8AYKoV9/Kqer+ZbrX/AEQ9EbugahqNjxuILJYWt7u3j7ZZJQpGGccwBI5sd3brVk4HLNs49GcTiscV0+qLYHQZrsnMM0AoBQCgFAKAUAoDmcTXAtOHdUuD0itJX+SGnMFaaXELfS7OEDaOBEHwUCvn1w9VeT7v5lxorFKK7I+MNxHb8VQmWSZFa1CkxK52Lnrynpt0O2M+VWXgC/Rn6/Q4nF/9kfQuCAYiQd37P3enwruHJPpQCgFAKAUAoBQCgIz6SJez4K1NM4M6pbr75HVB/dXmctMXLomeoLMkupEAAoAGwGwr503l5Lmltg40UJm42iLQxyRi3hjBZgpRi7kYJx5DbO+PPFXDgKxat939Cu8Vf66Xb/pdUEQiiSNeigADOcfGuucw+lAKAUAoBQCgFAKAhfpLm5rfSdPxn1i9EjD9mJS+f5gnzFQuI1fDtJv2e8l2MNdxFEeG/vqi4LVyMcL2Ru+J7uWOXlb6uF0IDK8YUMwIz+0d/Drvg4unBVi09rK1xN5uH7C1h0rpnPFAKAUAoBQCgFAKArXiu4F7xi4GCmnWqwg/8yQ87j+UR/Oq9x+tiEKS57nZ4RSzKVT2HPDFrhlH2UXf2k/oP7qr+lKkm1u/p9/A7GpueOhtcEzrFrl28sWYfWWBl27jCNRzDx8MHGdjvtV04XHFnD75lZvnm5n98i0B0qaRBQCgFAKAUAoBQHl2CKWY4UDJPkKAqHT5mvEm1Fzlr+d7rPTuMfqx8E5B8KpnFavi3cu23uLPw+n4duur3956tApaWXq7Ngn3bgfn8zjwqPc5WI8kb6OHmR0fR61ubq6JiUzNeTd/AbKhyACOoIxkEbefTNXSyjptYLsisXL1VpPuWYoAUADAA6Ct5oM0AoBQCgFAKAUBwOO7prbhXUORykk6C2Rh1DSsIx/dWurPw4Sn0TZ7pQ1zUVzIMFWOIIowoGAOlUGDcqmS3ySjDB5tOUwIyggN3t9ic75+PX41mvnxGn99vYKf7Edn0ZaYba0W+ESyetl3MjOS8XMxYbHopBH2dvGr9S2pRXZfIqFV5nJ92WDXs8CgFAKAUAoBQCgIb6QbnM2mWAyQzvcv5YQBQD/E4I/drl8Yq+Hatc3sdDhtPXXT6EXu+b1d1UAsw5QD0OfOqnapeJl8tyw1v2YR7+xH3jnlXc4xnbrWrznse/KJ0vRDLNJo0JkncKLdAIHVQUJ38ySOp3x1PdGa+i4SWEUtvO5YtAKAUAoBQCgFADQFdcVSmfim63ysEEMIHk3edvyZPlVa49V/NCn6s7nCIbSmcm4UyNGgJA5uZj7B+vT4muLReiMpff3/AEdWotTSPOov2WnXcn4IXb5Ka80FqrRXdGarxTb7Eq9Hsgn0W2guIbRWjhQKqXBkYAAeajGPYdtq+hMppM6wBQCgFAKAUAoDB6UBWN/KLjVtRnBJ57p1/k+r/wBFUzjFTVdyS5YRZ+Gw026zzNMoWulbOAqHbzJ/Tf51D1ONHHVknTmpnoanET9noGotvtbv091e7Farqmu6PN08UJ+jJxwXLp9vp4WDTxZYwoLWhheVseRyT78/AYq+FRJdQCgFAKAUAoBQHznkWGCSVvsopY+4b0BVVuPqEJ6leZifEncn518+uqniV5z6tlxoQ0Uox6I8iPN00hOypyr7M9f6CvU5rwYxRiMf1G2afEefoO8CnlLIFBxnGSB/mt3Dd7un6mq+/jz9Cd8CwSLYs13O09zsHy6OqnoSnKxABwNtj51eSqksoYFAKAUAoBQCgOVxS/Jw5qODgvbtGD5Fhyj+taq8tNKUuifyNlFZqRXdFf189ZckYHU17k8xR5S3bOVxU6JoN00gzGDHzj9ntFz+VTuFfy4ffUicQ/jyJ/wXMLuzR7YO0K5X1gnJYA45Tuc9PteXlV2KuSyhgUAoBQCgFAKA4fGbcvD83tkiH/sWol9LTa1H2ZItFmvBdyDVQ2W4VgHN4jyNDvHHVI+0HvUhv8VP4bLF1T9SJfLNvP0LA4RvluowGs7m3m5MsHUhTv1B5Vzn3VeSqEnoBQCgFAKAUAoDg8bf8Pyf9aH/AOi1D4h/EqehJsv5EPUhAqiMtorAPhqEHrNhcwDrJEyD3kGt1vPRVjLo0a60dVOS7Eh9HLxnTbOeBrl0ntlYFphKD44YhRgjJ+0Sfbsc/Qim8id0AoBQCgFAKAUBxuL15uHrr9kxv8nU/wCKjXizbVF2fyN1s8VoPuiB1QWXBCsAVlBmtwc9ygnhSCKNLGXsGuZUGWAfKqGLqAOVl6efTNfQLWoqlGE1zSKfXhoqyj3LcjYsoJUrkdDW80nqgFAKAUAoBQGnrFt65pN5ajrNA6D3lSK8yipRcXzMxlpaZWkbc8av05gDXzypFwm4vkXODUopo9V4PQoDmC4+iNc7ft1to7xVHbbgq6A5GQp2K464GEO9WzgdxrpOk/NfJlf4rRcaiqLn8yz+Gr+3vLQLbnnVAPrdsSZ8QRjm94GD4Gu4ck7NAKAUAoBQCgMHpQFYXsHqmoXdsduynYKPJSeZf/FhVJ4rR8O7l0e/v/stVhU128e2x8q5pMFMA1tRs1vrRoGYo2Q8cijeN1OVYe4gGpVpcSt6qqR5Gi4oxrU3BnZ4O1x3bGpKyXsORdNJIqxp+0u4BU42JHN55xteqVaFaCqQ8mVSrTlTm4S8ywYZVmjWRCSrdCRithrPpQCgFAKAUAoCHcZ6dIL6G+hQssqiKTHUMD3fnkj4LXC43bucI1Irdbe/y+PzOtwusoSlBvz39xo2HD99dNE0kRjhZu8zbEDx2rlW3Ca9VpyWE/eT6/EaVPKi8v75kgHCenYHen/nH6V2/wAEte/vOZ+K3HYz/snp/wCKf+YfpWPwS17+8fitfsYThHTFuIp/rjJGcqefHw28KlW1hTtpN029+5or3k66xNI7kUSQoEjBAHmSf61OIh7oBQCgFAKAUBjGaAzQCgFAKAUAoBQH/9k=',
    productName: '아주 긴 이미지',
    optionList: ['옵션1'],
    price: 15300000,
    sale: 67,
    salePrice: 4990000,
    hash: '서울 스토어 따라잡기'
  },
  {
    productId: 1,
    productImage: 'https://images.seoulstore.com/products/ac1652817fa7bf034ce3cfb7a36a882b.jpg',
    productName: '프랑켄모노',
    optionList: ['[단독특가] 프랑켄모노 양털 후리스 집업'],
    price: 153000,
    sale: 67,
    salePrice: 49900,
    hash: '세일 세일'
  },
  {
    productId: 1,
    productImage: 'https://images.seoulstore.com/products/ac1652817fa7bf034ce3cfb7a36a882b.jpg',
    productName: '프랑켄모노',
    optionList: ['[단독특가] 프랑켄모노 양털 후리스 집업'],
    price: 153000,
    sale: 67,
    salePrice: 49900,
    hash: '세일 세일'
  },
  {
    productId: 1,
    productImage: 'https://images.seoulstore.com/products/ac1652817fa7bf034ce3cfb7a36a882b.jpg',
    productName: '프랑켄모노',
    optionList: ['[단독특가] 프랑켄모노 양털 후리스 집업'],
    price: 153000,
    sale: 67,
    salePrice: 49900,
    hash: '세일 세일'
  },
];

const onClickProduct = productId => Router.push(`/product/${productId}`);

function Ranking() {
  return (
    <div>
      <Navigation />
      <div className="page-title row-between">
        <strong>Ranking</strong>
        <div><button>1</button><button>2</button></div>
      </div>
      <HorizontalMenu list={rankingMenuList} withBorder />

      <div className="product-list-container">
        {productList.map((product, index) =>
          <div key={product.productName + index} role="button" onClick={() => onClickProduct(product.productId)}>
            <div className="product-item-image-wrap">
              <img src={product.productImage} />
            </div>
            <div className="rank row-between">
              <strong>{index + 1}위</strong>
              <strong>-</strong>
            </div>
            <div className="product-item-product-name">{product.productName}</div>
            {product.optionList && product.optionList.map(option => <div key={option} className="product-item-option">{option}</div>)}
            <div className="product-item-price-info">
              <span className="sale-price">{product.sale}% {product.salePrice.toLocaleString()}</span>
              <span className="price">{product.price.toLocaleString()}</span>
            </div>
            <div className="product-item-hash">{product.hash.split(' ').map((hash, index) => <span key={hash + index}>{`#${hash} `}</span>)}</div>
          </div>
        )}
      </div>
    </div>
  );
}

export default Ranking;