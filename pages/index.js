import { useState, useEffect } from 'react';
import Router from 'next/router';

import Navigation from "../components/Nav/Navigation";
// import Icon from "../../components/Icon";
// import ProductList from '../../components/Feed/ProductList';

import { findProductList } from '../api/product';
import Carousel from '../components/Carousel';
import { Flex, Box, Image } from 'rebass/styled-components';

function Index() {
  const [productList, setProductList] = useState([]);

  useEffect(() => {
    findProductList().then(list => {
      setProductList(list);
    });
  }, []);

  return (
    <div>
      <Navigation />
      
      <Carousel />
      <Carousel />
      <Flex sx={{ overflowX: 'auto' }}>
        {Array(3).fill().map((i, k) =>
          <Box key={k} sx={{ minWidth: 'auto' }}>
            <Flex
              alignItems='center'
              mr='10px'
              bg='blue'
              width={['100px', '200px', '300px']}
              height={['100px', '200px', '300px']}
            >
              <Image src="/static/images/koco_logo@4x.png" />
            </Flex>
          </Box>
        )}
      </Flex>
      {/* <div
        className="partner-visual"
        style={{ backgroundImage: 'url("/static/img/image10.jpg")', backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat' }}
      >
        <div className="partner-visual-tag01">K-fashion Free Market</div>
        <div className="partner-visual-tag02">
          매일 <span>새로운 모습</span>을<br />
          보여줘야 하는 당신을 위해
        </div>
        <div className="partner-visual-tag03">
          <span>지금, 단 10초만에 셀럽 멤버쉽 가입</span>
          <Icon className="arrow-right-white">btn_arrow_right</Icon>
        </div>
      </div>
      <div className="divider"></div>
      <PartnerIndexSection
        title="인기상품"
        desc="다른 셀러들이 많이 받은 상품입니다."
        path="partner/product/popular"
        productList={popularList}
      />
      <div className="divider"></div>
      <PartnerIndexSection
        title="신상품"
        desc="새로운 상품을 확인해 보세요."
        path="partner/product/recent"
        productList={recentList}
      /> */}
    </div>
  );
}

function PartnerIndexSection({ title, desc, path, productList }) {
  const onClickSectionTitle = () => Router.push('/' + path);
  return (
    <div className="hot-item">
      <div onClick={onClickSectionTitle} className="partner-index-section-title-wrap">
        <div>
          <span className="hot-item-title">{title}</span>
          <span className="hot-item-explanation">{desc}</span>
        </div>
        <Icon style={{ width: '6px', height: '11px' }}>btn_arrow_right</Icon>
      </div>
      <ProductList list={productList} />
    </div>
  );
}

Index.getInitialProps = ({ query }) => query;

export default Index;