import React, { useEffect } from 'react';
import App from 'next/app';
import withReduxStore from '../utils/with-redux-store';
import { Provider, connect } from 'react-redux';
import { ThemeProvider } from 'emotion-theming'
import theme from '@rebass/preset'

import Lottie from 'lottie-web';
import animationData from '../static/lottie/51-preloader.json';

import { initAuthByStorage } from '../store/actions/app';
import '../config/i18n';

import 'normalize.css';
import "react-alice-carousel/lib/alice-carousel.css";
import '../styles/index.scss';


function Loading({ loading }) {
  useEffect(() => {
    Lottie.loadAnimation({
      container: document.getElementsByClassName('lottie-loading')[0],
      renderer: 'svg',
      loop: true,
      autoplay: true,
      animationData
    });
  }, []);

  return (
    <div className={`lottie-loading-wrap`} style={!loading ? { display: 'none' } : null}><div className={`lottie-loading`}></div></div>
  );
}
const mapStateToProps = ({ app: appState }) => ({ loading: appState.loading });
const AppLoading = connect(mapStateToProps, null)(Loading);

class NextApp extends App {
  componentDidMount() {
    this.props.reduxStore.dispatch(initAuthByStorage());
  }

  componentWillUnmount() {
  }

  render() {
    const { Component, pageProps, reduxStore } = this.props;

    return (
      <Provider store={reduxStore}>
        <ThemeProvider theme={theme}>
          <Component {...pageProps} />
        </ThemeProvider>
        <AppLoading />
      </Provider>
    )
  }
}

export default withReduxStore(NextApp);