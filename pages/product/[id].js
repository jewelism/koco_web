import { Flex, Box, Image, Text } from 'rebass/styled-components';
import Navigation from '../../components/Nav/Navigation';
import { BORDER_LIGHT, SILVER_COLOR, BLACK_COLOR, SIG_COLOR } from '../../constants/color';
import Carousel from '../../components/Carousel';
import { useRouter } from 'next/router';

function Product() {
  const router = useRouter();
  const { id } = router.query;

  return (
    <div>
      <Navigation />
      <Flex mx={[3, 4, 5]} flexDirection="column" alignItems="center">
        <Box width={1}><Flex justifyContent="center">
          <Carousel />
        </Flex></Box>
        {/* <Box width={1}><Flex alignItems="center">
          <Box width={1 / 2}>
            <Flex alignItems="center">
              <Box size={[30, 40, 50]}>
                <Image
                  sx={{
                    border: BORDER_LIGHT,
                    borderRadius: '50%'
                  }}
                  src="https://images.seoulstore.com/channels/9a98fb2b3b9c34297f051d30b40fdf79.jpg" />
              </Box>
              <Text
                ml={3}
                fontSize={1}
                fontWeight={600}
                color='primary'>
                롬앤
              </Text>
            </Flex>
          </Box>
          <Box width={1 / 2}><Flex justifyContent="flex-end">
            팔로우
          </Flex></Box>
        </Flex></Box> */}
        <Box width={1} sx={{ my: 2, borderBottom: BORDER_LIGHT }}></Box>
        <Box width={1}><Flex>
          <Box width={1 / 2}>
            @romandyou
            </Box>
          <Box width={1 / 2}>
            <Flex justifyContent="flex-end">
              Instagram
              </Flex>
          </Box>
        </Flex></Box>
        <Box width={1}>
          <Flex color={SILVER_COLOR}>
            <Box>#신상</Box>
            <Box>#세일</Box>
          </Flex>
          <Text
            fontSize={4}
            fontWeight='bold'
            color={BLACK_COLOR}>
            [롬앤] 제로 레이어 립스틱 6color
          </Text>
          <Flex>
            <Text
              fontSize={2}
              fontWeight='bold'
              color={SIG_COLOR}
              mr={1}>
              41%
            </Text>
            <Text
              fontSize={2}
              fontWeight='bold'
              color={SIG_COLOR}
              mr={1}>
              8,900
            </Text>
            <Text
              fontSize={2}
              fontWeight='bold'
              color={SILVER_COLOR}>
              15,000
            </Text>
          </Flex>
          <Text
            fontSize={1}>
            조회 893
          </Text>
          <Box width={1} sx={{ my: 2, borderBottom: BORDER_LIGHT }}></Box>
          <Text
            fontWeight='bold'
            fontSize={1}>
            구매 가능한 최저가
          </Text>
          <Flex>
            <Text
              fontSize={2}
              fontWeight='bold'
              color={SIG_COLOR}
              mr={1}>
              6,099
            </Text>
            <Text
              fontSize={2}
              color={SIG_COLOR}
              mr={1}>
              원
            </Text>
          </Flex>
          <Text color={SILVER_COLOR}>
            포인트, 쿠폰을 모두 적용하면!
          </Text>
          <Flex>
            <Text fontWeight={'bold'} mr={1}>
              최대 2,801원 할인
            </Text>
            <Text color={SILVER_COLOR}>
              받을 수 있어요!
            </Text>
          </Flex>
          <Box width={1} sx={{ my: 2, borderBottom: BORDER_LIGHT }}></Box>
          {id}
        </Box>
      </Flex>
    </div>
  );
}

export default Product;