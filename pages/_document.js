import Document, { Main, Head, NextScript } from 'next/document';
import NextHead from 'next/head';
import flush from 'styled-jsx/server';

export default class MyDocument extends Document {
  // static getInitialProps({ renderPage }) {
  //   const { html, head } = renderPage();
  //   const styles = flush();
  //   return { html, head, styles };
  // }

  render() {
    return (
      <html>
        <Head>
          <meta property="og:title" content="KOCO" />
          <meta property="og:description" content="Korea Cosmetic" />
          <meta property="og:image" content={`https://${process.env.LOCALE}.in-flower.com/static/icon/header_logo@3x.png`} />
        </Head>
        <NextHead>
          <title>KOCO</title>
          {/* {!isDev && <>
            <script dangerouslySetInnerHTML={{ __html: `
              !function(f,b,e,v,n,t,s)
              {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
              n.callMethod.apply(n,arguments):n.queue.push(arguments)};
              if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
              n.queue=[];t=b.createElement(e);t.async=!0;
              t.src=v;s=b.getElementsByTagName(e)[0];
              s.parentNode.insertBefore(t,s)}(window,document,'script',
              'https://connect.facebook.net/en_US/fbevents.js');
              fbq('init', '1482653775209522'); 
              fbq('track', 'PageView');
            `}}>
              
            </script>
            <noscript dangerouslySetInnerHTML={{ __html: `
              <img height="1" width="1" 
                src="https://www.facebook.com/tr?id=1482653775209522&ev=PageView
                &noscript=1"/>
              `}}>
            </noscript>
          </>} */}
        </NextHead>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}