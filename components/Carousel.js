import AliceCarousel from 'react-alice-carousel';
import { Image, Box, Flex } from 'rebass/styled-components';

const handleOnDragStart = e => e.preventDefault();

function Carousel(props) {
  return (
    <Box my={[1, 2, 3]}>
      <AliceCarousel mouseDragEnabled buttonsDisabled {...props}>
        {Array(3).fill().map((i, k) =>
          <Flex key={k} justifyContent="center" sx={{ width: '100%' }}>
            <Image
              src={"https://images.seoulstore.com/design/2c1b9f25073a3db543520aceffe79853.gif"}
              sx={{
                width: '100%',
                maxWidth: '1000px',
                borderRadius: '4px'
              }}
              onDragStart={handleOnDragStart}
            />
          </Flex>
        )}
      </AliceCarousel>
    </Box>
  );
}

export default Carousel;