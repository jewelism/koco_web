export default function Icon({ children, type = 'svg', ...props }) {
  return (
    <img src={`/static/icons/${children}.${type}`} {...props} />
  );
}