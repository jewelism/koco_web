import { useState } from 'react';
import Router from 'next/router';
import Icon from '../Icon';
import HorizontalMenu from './HorizontalMenu';

const onClickLogo = () => Router.push('/');

const categoryList = [
  { text: 'Home', path: '/' },
  { text: 'Ranking', path: '/ranking' },
  { text: 'Event', path: '/event' },
  { text: 'Category', path: '/category', color: '#f70061' },
  { text: 'Brand', path: '/brand' },
  // { text: 'pre-sale', color: '#f70061' },
];

function Navigation() {
  return (
    <div className="top-nav-container">
      <TopNav />
      <HorizontalMenu list={categoryList} />
    </div>
  );
}

function TopNav() {
  return (
    <div className="top-wrap">
      <div onClick={onClickLogo} className="top-logo-wrap"><img alt="KOCO" src="/static/images/koco_logo@4x.png" /></div>
      <TopLeftNav />
    </div>
  );
}

function TopLeftNav() {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <div className="menu-wrap">
        <a>1</a>
        <a>2</a>
        <a>3</a>
        <a>4</a>
        <a onClick={() => setIsOpen(!isOpen)}><Icon>icon_menu</Icon></a>
      </div>
      <div className={isOpen ? 'menu-open-container' : 'display-hidden'} style={{ overflow: 'auto' }}>
        <Menu setIsOpen={setIsOpen} />
      </div>
    </>
  );
}

const list = [
  { text: 'Sign in', path: '/login', color: 'black' },
  { text: 'Sign up', path: '/join', color: 'black' },
];

const onClickHome = (setIsOpen) => {
  Router.push('/');
  setIsOpen(false);
};

function Menu({ setIsOpen }) {
  return (
    <div>
      <div className="row-between" style={{ marginBottom: '20px' }}>
        <HorizontalMenu list={list} withBorder />
        <div className="menu-wrap">
          <a onClick={() => onClickHome(setIsOpen)}>Home</a>
          <a onClick={() => setIsOpen(false)}>X</a>
        </div>
      </div>
      <img src="https://images.seoulstore.com/design/d75d07b67a1406851f4ca663e6fde91a.jpg" width={'100%'} />
      <img src="https://images.seoulstore.com/design/2d93d4308fa65f2630d7098384be1ee5.jpg" width={'100%'} />
      <img src="https://images.seoulstore.com/design/6c56ad0a206b06d9a6c5c33c78b4186b.jpg" width={'100%'} />
      <img src="https://images.seoulstore.com/design/223dd374140d15fa33f5c5fb260c55cc.jpg" width={'100%'} />
      <h1>promotion</h1>
      <img src="https://images.seoulstore.com/design/e6c11336553955283ae0aa0d8c96fdf8.jpg" width={'100%'} />
      <img src="https://images.seoulstore.com/design/30a0de69aba8f4b4d305d108df9e37f3.jpg" width={'100%'} />
      <h1>one + one</h1>

      <div className="flex-center col">
        <div className="divider-fat"></div>
        <div className="menu-bottom-container">
          <div>카테고리</div>
          <div>브랜드</div>
          <div>신상</div>
          <div>신상</div>
          <div>신상</div>
          <div>신상</div>
          <div>신상</div>
          <div>신상</div>
          <div>신상</div>
          <div>신상</div>
        </div>
      </div>
    </div>
  );
}


export default Navigation;