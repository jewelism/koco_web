import Router from 'next/router';

function HorizontalMenu({ list, withBorder }) {
  return (
    <div className={withBorder ? 'category-wrap-divider' : 'category-wrap'}>
      {list.map((item, index) => <span key={item.text + index} role="button" onClick={() => Router.push(item.path)} style={{ color: item.color || 'inital' }}>{item.text}</span>)}
    </div>
  );
}

export default HorizontalMenu;