const express = require('express');
const helmet = require('helmet');
const next = require('next');
const https = require('https');
const { parse } = require('url');
const fs = require('fs');
const cookieParser = require('cookie-parser');

const app = next({ dev: true });
const handle = app.getRequestHandler();

const httpsOptions = {
  key: fs.readFileSync('./dev/key.pem'),
  cert: fs.readFileSync('./dev/cert.pem')
};

function createServer() {
  const server = express();
  server.use(helmet());
  server.use(cookieParser());

  server.get('*', (req, res) => {
    const parsedUrl = parse(req.url, true);
    handle(req, res, parsedUrl);
  });
  return server;
}

app
  .prepare()
  .then(() => {
    const server = createServer();
    https.createServer(httpsOptions, server).listen(3000);
    console.log('dev > Ready on https://localhost:3000');
  })
  .catch(({ stack }) => {
    console.error(stack);
    process.exit(1);
  });

