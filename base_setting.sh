#!/bin/bash

touch ./env;
chmod 755 ./env;

/bin/bash ./env_setting.sh;
/bin/bash ./ssh_setting.sh;

curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -;
sudo apt-get install -y nodejs;

npm i -g pm2;
npm i;

/bin/bash ./deploy.sh;