import i18n from "i18next";
import {initReactI18next} from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector';

import en from '../static/locales/en';
// import ko from '../static/locales/ko';
// import in from '../static/locales/in';

// console.log(process.env.LOCALE);
const resources = {
  en: {translation: en},
  // ko: {translation: ko},
  // in: {translation: in},
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  // .use(LanguageDetector)
  .init({
    lng: process.env.LANGUAGE,
    fallbackLng: 'en',
    resources,
    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

export default i18n;