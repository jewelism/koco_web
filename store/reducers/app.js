import actionTypes from '../types/app';
import { DEFAULT_AUTH_ID, AUTH_STORAGE_KEY } from '../../constants/auth';

const initialState = {
  auth: { id: DEFAULT_AUTH_ID },
  loading: false,
  category: 'Feed',
  lastUpdate: 0,
  light: false,
  count: 0
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.INIT_AUTH_BY_STORAGE:
      const storageAuthString = localStorage.getItem(AUTH_STORAGE_KEY);
      const auth = JSON.parse(storageAuthString || `{"id": "${DEFAULT_AUTH_ID}"}`);
      return { ...state, auth};
    case actionTypes.SET_AUTH:
      return { ...state, auth: action.auth };
    case actionTypes.START_LOADING:
      return { ...state, loading: true };
    case actionTypes.STOP_LOADING:
      return { ...state, loading: false };
    case actionTypes.SET_CATEGORY:
      return { ...state, category: action.category };
    default:
      return state
  }
}
