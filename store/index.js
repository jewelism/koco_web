import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'

import reducer from './reducers';

const store = createStore(
  reducer,
  {},
  composeWithDevTools(applyMiddleware())
);

export function initializeStore() {
  return store;
}

export default store;