import actionTypes from '../types/app';

export const initAuthByStorage = () => ({ type: actionTypes.INIT_AUTH_BY_STORAGE });
export const setAuth = (auth = {}) => ({ type: actionTypes.SET_AUTH, auth });

export const startLoading = () => ({ type: actionTypes.START_LOADING, loading: true });
export const stopLoading = () => ({ type: actionTypes.STOP_LOADING, loading: false });

export const setCategory = category => ({ type: actionTypes.SET_CATEGORY, category });