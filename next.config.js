const withCSS = require('@zeit/next-css');
const withSass = require('@zeit/next-sass');

require('dotenv').config();
const path = require('path');
const Dotenv = require('dotenv-webpack');

// module.exports = withSass()
module.exports = (withCSS(withSass({
  distDir: '_next',
  crossOrigin: 'anonymous',
  // exportPathMap() {
  //   const path = {};
  //   ['/search', '/product', '/test'].forEach(page => {
  //     path[page] = {page};
  //   });
  //   return {
  //     '/': {page: '/index'},
  //     ...path,
  //   }
  // },
  webpack(config) {
    config.plugins.push(
      new Dotenv({
        path: path.join(__dirname, '.env'),
        systemvars: true
      })
    );
    config.module.rules.push({
      test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000,
          name: '[name].[ext]'
        }
      }
    });
    return config;
  },
})));